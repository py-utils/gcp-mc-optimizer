from bs4 import BeautifulSoup
from typing import Generator


class scraper:

    def __init__(self) -> None:
        self.__domain = "https://cloud.google.com"

    def __html_parse(
        self,
        page: str
    ) -> BeautifulSoup:
        """
        Returns parsed html page into a soup
        """

        return BeautifulSoup(
            page,
            'html.parser'
        )

    def __extract_configs(
        self,
        key: str,
        configs: str = "configs.ini"
    ) -> str:
        """
        Extract near-sense key from configs
        """

        from configparser import ConfigParser

        parser = ConfigParser()
        parser.read(configs)

        # NEAR SENSE DISAMBIGUATION FOR `KEY`

        return parser.get('URL', key)

    def __handle_staging(
        self
    ) -> str:
        """
        Handle staging folder
        """

        from os import getcwd, makedirs
        from os.path import join, exists, isfile
        from shutil import rmtree

        dir_path = join(getcwd(), 'staging')

        if not exists(dir_path) and not isfile(dir_path):
            makedirs(dir_path)
        else:
            rmtree(dir_path)  # platform independent instead of using os.system
            makedirs(dir_path)

        return dir_path

    def get_families(
        self,
        landing: str = "{0}/compute/docs/machine-types"
    ) -> Generator[str, None, None]:
        """
        This method will get all families' page from a landing page
        Producer Setup
        """

        import requests

        staging_path = self.__handle_staging()

        soup = self.__html_parse(
            requests.get(
                landing.format(
                    self.__domain
                )
            ).text
        )

        for alink in soup.find_all('a'):
            if alink.text.__contains__("machine family"):
                yield (
                    "{0}{1}".format(
                        self.__domain,
                        alink.get('href')
                    ),
                    staging_path
                )

    def extract_tables(
        self,
        pageurl: str,
        staging: str
    ) -> str:
        """
        This method extracts machine spec tables from pageurl
        """

        import requests
        import os
        import sys
        import logging
        import csv
        import re

        pagesoup = self.__html_parse(
            requests.get(
                pageurl
            ).text
        )

        for table in pagesoup.find_all('table'):

            try:

                # Examine theader - is it what we are looking for?
                header = [h.text for h in table.thead.find_all('th')]
                if header.__contains__("Machine types"):

                    # Get tbody
                    body = [[t.text for t in f.find_all(
                        'td')] for f in table.tbody.find_all('tr')]

                    # Mode for deciding name of itemtable
                    candidates = [re.search("(\w+-\w+)", b[0]).group(1)
                                  for b in body if b.__len__() > 0]
                    id = max(set(candidates), key=candidates.count)

                    # Dump itemtable
                    with open("{0}/{1}.csv".format(staging, id), "w") as file:
                        writer = csv.writer(file)
                        writer.writerow(header)
                        writer.writerows([b for b in body if b != []])

                else:

                    pass

            except:

                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(
                    exc_tb.tb_frame.f_code.co_filename)[1]
                logging.warning(
                    f"{exc_type};{exc_obj} at {fname} line {exc_tb.tb_lineno}")

        return staging
