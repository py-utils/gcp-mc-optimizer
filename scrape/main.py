from utils import scraper

if __name__ == "__main__":

    # Init scrapper object
    scraper = scraper()

    # Extract info pages
    for pagelink, staging_path in scraper.get_families():
        scraper.extract_tables(
            pagelink,
            staging_path
        )
